# Sharing #

The *_shared* folder is for receiving files shared with you.

### What is Sharing? ###

* Sharing is a collaboration feature within Optilogic
* Any Optilogic user may receive shared files regardless of subscription level
* Only those with a **Standard** or higher subscription level can share files with others

### How do I Share files in Atlas? ###

* You must have a **Standard** subscription or higher
* In the Explorer pane, right-click on a file 
* In the context menu, click **Share**
* In the share dialog popup enter the recipient(s)
* Note: multiple recipients can be entered by separating with a space or comma

### How do I do Share files using the API? ###

* You must have a **Standard** subscription or higher
* Use the /v0/{workspace}/file/share endpoint as documentated in the API documentation